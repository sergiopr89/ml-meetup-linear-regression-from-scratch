# %%
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_boston

# %%
boston_dataset = load_boston()
target_index = 0
rm_index = 5
print(boston_dataset.DESCR)

# %%
X, y = (boston_dataset.data, boston_dataset.target)
y = np.reshape(y, (-1, 1)) # Convert 1D (of N samples) to 2D (of N_samples x 1)
print(f'X shape --> {X.shape}')
print(f'y shape --> {y.shape}')

# %%
# Obtain the set division (70/30) length
train_length = round(X.shape[0] * 0.7)
test_length = X.shape[0] - train_length
print(f'Train length --> {train_length}')
print(f'Test length --> {test_length}')

# %%
# Create the two sets, for inputs "X" and targets "y"
X_train = X[0:train_length, ]
X_test = X[train_length-1:-1, ]
y_train = y[0:train_length, ]
y_test = y[train_length-1:-1, ]
print('Training set:')
print(f'\tX shape --> {X_train.shape}')
print(f'\ty shape --> {y_train.shape}')
print('Test set:')
print(f'\tX shape --> {X_test.shape}')
print(f'\ty shape --> {y_test.shape}')


# %%
def plot_data_xy(x, y, title, theta=None):
    plt.scatter(x, y)
    plt.title(title)
    plt.xlabel('x')
    plt.ylabel('y')
    if theta is not None:
        plt.plot(x, x * theta[1] + theta[0], color='red')
    plt.show()


# %%
# Plot full data set
plot_data_xy(X[:,rm_index], y[:,target_index], 'Data set')

# %%
# Plot training set
plot_data_xy(X_train[:,rm_index], y_train[:,target_index], 'Training set')

# %%
# Plot test set
plot_data_xy(X_test[:,rm_index], y_test[:,target_index], 'Test set')
# We see a data mismatch, we can address it by retrieving more real data or sitetizing new data.

# %%
# For the shake of the example I will cheat and just merge a bit of data from the training set (don't do it in real projects!!!)
chunk_of_X_train = X_train[0:int(train_length*0.2), ]
chunk_of_y_train = y_train[0:int(train_length*0.2), ]
X_test = np.concatenate((X_test, chunk_of_X_train))
y_test = np.concatenate((y_test, chunk_of_y_train))
print('Training set:')
print(f'\tX shape --> {X_train.shape}')
print(f'\ty shape --> {y_train.shape}')
print('Test set:')
print(f'\tX shape --> {X_test.shape}')
print(f'\ty shape --> {y_test.shape}')

# %%
# Plot test set
plot_data_xy(X_test[:,rm_index], y_test[:,target_index], 'New test set')
# We still see some outliers we could get rid with other ML methods, but note real data can have also those outliers,
#the main idea is to know our data and make coherent predictions

# %%
def add_bias_x0(X_var):
    """ Adds x sub 0 = 1 for each sample """
    x_0 = np.ones((X_var.shape[0], 1))
    return np.concatenate((x_0, X_var), axis=1)


# %%
def normal_equation(X_var, y_var):
    """ Input a train set and return the fitted parameters"""
    X_var = add_bias_x0(X_var)
    #X_var_T = X_var.T
    #X_var_pinv = np.linalg.pinv(np.dot(X_var_T, X_var))
    #X_var_pinv = np.dot(X_var_pinv, X_var_T)
    X_var_pinv = np.linalg.pinv(X_var)
    theta = np.dot(X_var_pinv, y_var)
    return theta


# %%
def predict(X_var, theta, add_bias_term=True):
    """ Get a set of inputs, parameters and return predictions """
    if add_bias_term:
        X_var = add_bias_x0(X_var)
    y_hat = np.dot(X_var, theta)
    return y_hat


# %%
theta_ne = normal_equation(X_train[:, rm_index:rm_index+1], y_train)

# %%
print('Parameters obtained in normal equation')
print(f'\tTheta sub 0 (bias parameter) --> {theta_ne[0][0]}')
print(f'\tTheta sub 1 (feature coefficient) --> {theta_ne[1][0]}')
plot_data_xy(X_train[:,rm_index], y_train[:,target_index], 'Training set', theta_ne)

# %%
y_hat_ne = predict(X_train[:, rm_index:rm_index+1], theta_ne)

# %%
print(f'Prediction result example: y={y_train[100,0]} | y_hat={y_hat_ne[100,0]}')


# %%
def init_parameters(num_of_features):
    """ Get a num of features and return a Nx1 array (column vector) """
    return np.zeros((num_of_features+1, 1)) # Add +1 for the bias term


# %%
theta_gd = init_parameters(X_train[:, rm_index:rm_index+1].shape[1])
print(f'Theta_gd shape --> {theta_gd.shape}')
print(f'Theta_gd:\n{theta_gd}')


# %%
def compute_cost(y_var, y_hat):
    """ Linear regression cost function (Half MSE) """
    sample_size = y_var.shape[0]
    frequency_coef = 1/(2*sample_size) 
    squared_errors = np.square(np.subtract(y_var, y_hat)).sum()
    mse = np.multiply(squared_errors, frequency_coef)
    return mse


# %%
def gradient_descent(X_var, y_var, theta, alpha=10**-3, iterations=10**4):
    """ Compute the gradient descent with a training data set and theta parameters, return updated parameters and cost history """
    sample_size = y_var.shape[0]
    cost_history = []
    X_var = add_bias_x0(X_var)
    y_hat = predict(X_var, theta, add_bias_term=False)
    cost = compute_cost(y_var, y_hat)
    cost_history.append(cost)
    for i in range(iterations):
        # Let's do gradient descent
        theta = theta - (alpha/sample_size) * X_var.T @ (y_hat - y_var)
        # Compute new prediction cost
        y_hat = predict(X_var, theta, add_bias_term=False)
        cost = compute_cost(y_var, y_hat)
        cost_history.append(cost)
    return theta, np.array(cost_history)


# %%
#theta_gd_new, cost = gradient_descent(X_train[:, rm_index:rm_index+1], y_train, theta_gd)
theta_gd_new, cost = gradient_descent(X_train[:, rm_index:rm_index+1], y_train, theta_gd, alpha=5*10**-4, iterations=10**6)

# %%
print('Parameters obtained in normal equation')
print(f'\tTheta sub 0 (bias parameter) --> {theta_ne[0][0]}')
print(f'\tTheta sub 1 (feature coefficient) --> {theta_ne[1][0]}')
print('Parameters obtained in gradient descent')
print(f'\tTheta sub 0 (bias parameter) --> {theta_gd_new[0][0]}')
print(f'\tTheta sub 1 (feature coefficient) --> {theta_gd_new[1][0]}')
print(f'GD Cost --> {cost[-1:][0]}')
plot_data_xy(X_train[:,rm_index], y_train[:,target_index], 'Training set (NE)', theta_ne)
plot_data_xy(X_train[:,rm_index], y_train[:,target_index], 'Training set (GD)', theta_gd_new)
